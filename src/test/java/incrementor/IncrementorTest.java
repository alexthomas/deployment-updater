package incrementor;

import incrementor.Incrementor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class IncrementorTest {

    @Spy
    private Incrementor incrementor;

    //region setup
    private static final String TEST_FILE_PATH = "test-file.yaml";
    private static final String INITIAL_FILE_PATH = "test-initial-file.yaml";
    private static final String TEST_PROJECT_PATH = "test-project";


    @BeforeEach
    void setUp() {
        InputStream initialFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(INITIAL_FILE_PATH);
        try {
            Files.createDirectories(Path.of(TEST_PROJECT_PATH));
            byte[] bytes = initialFileStream.readAllBytes();
            Files.write(Path.of(TEST_PROJECT_PATH, TEST_FILE_PATH), bytes, StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void cleanUp() {
        try {
            Files.walk(Path.of(TEST_PROJECT_PATH))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
            Files.deleteIfExists(Path.of(TEST_PROJECT_PATH, TEST_FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //endregion

    @Test
    void handleArgs() {
        Mockito.doNothing().when(incrementor).updateContainerVersion(anyString(), anyString(), anyString(), anyString(), anyString());
        incrementor.handleArgs(new String[]{"pj", "dp", "dpf", "i", "nv"});
        Mockito.verify(incrementor, times(1)).updateContainerVersion("pj", "dpf", "dp", "i", "nv");
    }

    @Test
    void loadDocuments() {
        String chartPath = TEST_PROJECT_PATH + "/" + TEST_FILE_PATH;
        Iterable<Object> documents = incrementor.loadDocuments(chartPath);
        AtomicInteger counter = new AtomicInteger();
        Set<String> actualTypes = new HashSet<>();
        documents.forEach(x -> {
            counter.incrementAndGet();
            actualTypes.add(((Map<String, String>) x).get("kind"));
        });
        Set<String> expectedTypes = new HashSet<>(Set.of("Deployment", "Service"));
        assertEquals(2, counter.get());
        assertEquals(expectedTypes,actualTypes);
    }

    @Test
    void updateContainerVersion() throws IOException {
        List<Object> documents = List.of(new HashMap<>(),new HashMap<>());
        doReturn(documents).when(incrementor).loadDocuments(anyString());
        doReturn("Service","Deployment").when(incrementor).getElement(anyMap(),eq("kind"));
        doReturn("other-name","real-name").when(incrementor).getElement(anyMap(),eq("metadata.name"));
        List<Map<String,String>> containers = List.of(new HashMap<>(),new HashMap<>());
        doReturn(containers).when(incrementor).getElement(anyMap(),eq("spec.template.spec.containers"));
        doReturn("fake-image:old-version","real-image:old-version").when(incrementor).getElement(anyMap(),eq("image"));
        incrementor.updateContainerVersion(TEST_PROJECT_PATH,"test-updated-file.yaml","real-name","real-image","new-version");
        assertTrue(containers.get(0).isEmpty());
        assertEquals("real-image:new-version",containers.get(1).get("image"));
    }

    @Test
    void getElement() {
        doReturn("").when(incrementor).getElement(anyMap(),any(),anyInt());
        incrementor.getElement(Collections.emptyMap(),"path.sub");
        verify(incrementor,times(1)).getElement(anyMap(),eq(new String[]{"path","sub"}),eq(0));
    }

    @Test()
    void getElement_withNoPath(){
        assertThrows(RuntimeException.class,()->{
            incrementor.getElement(Collections.emptyMap(),"");
        });
    }

    @Test
    void testGetElement() {
        Map<String, Object> map = Map.of("path", Map.of("sub", "value"));
        assertEquals("value",incrementor.getElement(map,new String[]{"path","sub"},0));
    }
}