package incrementor;

import lombok.SneakyThrows;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

public class Incrementor {

    private final Yaml yaml;

    public Incrementor() {
        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        yaml = new Yaml(options);
    }


    public static void main(String[] args) {
        Incrementor incrementor = new Incrementor();
        incrementor.handleArgs(args);
        System.exit(0);
    }

    void handleArgs(String[] args) {

        if (args.length < 5) {
            System.err.println("Missing arguments. Expected command format:\n" +
                    "<project> <deployment> <deploymentFile> <image> <newVersion> \n"+
                    "received "+ String.join(" ",args));
            System.exit(1);
            return;
        }
        String project = args[0];
        String deployment = args[1];
        String deploymentFile = args[2];
        String image = args[3];
        String newVersion = args[4];
        updateContainerVersion(project, deploymentFile, deployment, image, newVersion);
    }

    @SneakyThrows
    Iterable<Object> loadDocuments(String chartPath) {
        Iterable<Object> objects = yaml.loadAll(new InputStreamReader(new FileInputStream(chartPath)));
        return StreamSupport.stream(objects.spliterator(), false).toList();
    }

    @SneakyThrows
    void updateContainerVersion(String project, String deploymentFile, String deployment, String image, String newVersion) {
        String chartPath = project + "/" + deploymentFile;
        Iterable<Object> documents = loadDocuments(chartPath);

        for (Object document : documents) {
            String name = getElement(((Map<String, Object>) document), "metadata.name");
            String kind = getElement(((Map<String, Object>) document), "kind");
            if (!(kind.equals("Deployment") && name.equals(deployment))) {
                continue;
            }
            List<Map<String, Object>> containers = getElement(((Map<String, Object>) document), "spec.template.spec.containers");
            for (Map<String, Object> container : containers) {
                String containerImage = getElement(container, "image");
                if (containerImage.startsWith(image)) {
                    container.put("image", image + ":" + newVersion);
                    System.out.println("Updated image for " + deployment + " to " + newVersion);
                }
            }
        }
        yaml.dumpAll(documents.iterator(), new OutputStreamWriter(new FileOutputStream(chartPath)));
    }

    <T> T getElement(Map<String, Object> document, String path) {
        String[] segments = path.split("\\.");
        if (segments.length == 0 || segments[0].isEmpty())
            throw new RuntimeException("No path provided ");
        return getElement(document, segments, 0);
    }

    <T> T getElement(Map<String, Object> document, String[] path, int pathIndex) {
        Object element = document.get(path[pathIndex]);
        if (pathIndex + 1 == path.length) {
            return (T) element;
        }
        return getElement(((Map<String, Object>) element), path, pathIndex + 1);
    }
}
